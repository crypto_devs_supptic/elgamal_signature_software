const fs = require("fs")
const md5 = require("md5")
const md5File = require("md5-file")

class HashMessage {
    constructor() {

    }
    generateMessageDigest(data) {
        return md5(data)
    }
    generateFileDigest(data) {
        return md5File(data).then((hash) => {
            console.log('The MD5 sum of ' + data + ` is: ${hash}`)
        })
    }
}


module.exports = {
    HashMessage
}