const random = require('random')

class RandomInteger {
    constructor() {

    }
    generatePositiveInteger(start, stop) {
        return random.int(start, stop)
    }
}

module.exports = {
    RandomInteger
}