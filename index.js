const { RandomPrimeGenerator } = require("./elgamal_libs/RandomPrime")
const { HashMessage } = require("./elgamal_libs/HashMessage")
const { RandomInteger } = require("./elgamal_libs/RandomInteger")

console.log("bonjour tout le monde tout le monde doit cloner ca !!!!!")

//generateur d'un nombre premier
var a = new RandomPrimeGenerator()
var b = a.generateRandomPrime()
console.log(b)

//generateur d'un entier
var c = new RandomInteger()
console.log("the generated number is " + c.generatePositiveInteger(10, 16))

//hash un text/fichier
var d = new HashMessage()
d.generateFileDigest("./elgamal_libs/test.txt")
console.log("le hache du texte est " + d.generateMessageDigest("i am invincible"))